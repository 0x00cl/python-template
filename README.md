# Python template

This is a personal template that I'm sharing that saves time with having to setup argparse basic options and a sane logging format.

It can get you easily develop programs/scripts with useful options included such as being able to add arguments to your program and also log what your program/script does.

## Requirements

* Python 3 (3.6+) Tested with 3.6, 3.7, 3.10

## Execution

```
$ python main.py
```

Help output:
```
$ python main.py --help
usage: main.py [-h] [-d] [-v]

This is a useful tool that does a lot of things

options:
  -h, --help     show this help message and exit
  -d, --debug    set log level to DEBUG
  -v, --version  show program's version number and exit

python-template Copyright (C) 2022 Tomás Gutiérrez. This is free software, licensed
under GPLv3.
```

## Author

Tomás Gutiérrez <tgutierrezlet (at) gmail (dot) com> (https://0x00.cl)
