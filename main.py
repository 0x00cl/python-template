#!/usr/bin/env python
'''Python template'''
import argparse
import logging

def setup_argparse():
    '''
    Sets up the arguments for the program

    Returns:
        args (argparse.Namespace): parsed arguments
    '''
    parser = argparse.ArgumentParser(description="This is a useful tool that does a lot of things",
                                     epilog="python-template  Copyright (C) 2022  Tomás Gutiérrez. This is free software, licensed under GPLv3.")
    parser.add_argument('-d', '--debug', action="store_true", help='set log level to DEBUG')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s 0.1.0')
    return parser.parse_args()


def setup_logging(debug):
    '''
    Sets up logging module, setting log format and level

    Parameters:
        debug (bool): whether to show debug level in logs or not
    '''
    log_level = logging.INFO
    if debug:
        log_level = logging.DEBUG
    logging.basicConfig(format="%(asctime)s [%(levelname)-8s] (%(funcName)s:%(lineno)d) %(message)s", level=log_level)


def main():
    args = setup_argparse()
    setup_logging(args.debug)

    logging.info("Script starting")
    logging.debug("Debug!")
    #### Code ####
    logging.info("Finished running script")


if __name__ == '__main__':
    main()
